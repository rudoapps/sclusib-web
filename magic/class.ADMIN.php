<?php
include_once("config.php");
include_once("class.DB.php");
include_once("class.RENDER.php");

class admin{	
	var $template;	
	var $DB;
	function admin(){
		$this->DB 	  = new DB(_SERVER,_USERNAME,_PASSWORD,_DB);
		$this->RENDER = new render();
	}

	function isInfluencer($influencer){
		$this->DB->open();	
		$sql = "SELECT id, name, text, img_cover, twitter, instagram, url, view, username FROM `sb_influencer` WHERE url='".$influencer."' AND view=1";
		if (!$result = $this->DB->query($sql)) { 			
    		$this->DB->error();
    		$this->DB->close();
    		return false;
		}else{
			$this->DB->close();
			if($result->num_rows>0)
				return true;
			else
				return false;
		}

	}

	function getFriendlyURL($str, $replace=array(), $delimiter='-') {
		if( !empty($replace) ) {
			$str = str_replace((array)$replace, ' ', $str);
		}

		$clean = iconv('UTF-8', 'ASCII//TRANSLIT', $str);
		$clean = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $clean);
		$clean = strtolower(trim($clean, '-'));
		$clean = preg_replace("/[\/_|+ -]+/", $delimiter, $clean);

		return $clean;
	}

	function getSelectView($view){						
		$selected= 'selected="selected"';
		
			if($view==0){
				$select = '<option value="0" '.$selected.'>NO</option>';
				$select .= '<option value="1">SI</option>';
			}
			else{
				$select = '<option value="0">NO</option>';
				$select .= '<option value="1" '.$selected.'>SI</option>';
			}
		
		return $select;
	}

	function getSelectInfluencer($search=''){		
		$this->DB->open();		
		$select = '<option value="0">Seleccione un influencer</option>';
		$selected ='';
		$sql = "SELECT id, name,url, username FROM `sb_influencer` WHERE status=0";
		if (!$result = $this->DB->query($sql)) {}
		while($data=$result->fetch_assoc()){

			if($data['url']==$search) $selected= 'selected="selected"';
			else $selected ='';
			$select .= '<option value="'.$data['url'].'" '.$selected.'>'.$data['username'].'</option>';
		}
		$this->DB->close();
		return $select;
	}

	function getData($influencer){
		$this->DB->open();				
		$sql = "SELECT id, name, text, img_cover, twitter, instagram, url, view, username FROM `sb_influencer` WHERE url='".$influencer."'";
		if (!$result = $this->DB->query($sql)) { 			
    		$this->DB->error();
		}

		$data=$result->fetch_assoc();
			$return["id"]			=$data["id"];
			$return["name"]			=$data["name"];
			$return["url_profile"]	=$data["url"];
			$return["text"]			=$data["text"];
			$return["img_cover"]	=$data["img_cover"];
			if($data["img_cover"]!=''){
				$return["url_img_cover"]=_URL."magic/files/".$data["img_cover"];				
			}else{
				$return["url_img_cover"]=_URL."img/bg_admin.jpg";				
			}
			$return["view"]		=$this->getSelectView($data['view']);
			$return["twitter"]	=$data["twitter"];
			$return["instagram"]=$data["instagram"];
			$return["username"] =$data["username"];
		$this->DB->close();
		return $return;
	}

	function moreThan8(){
		$this->DB->open();				
		$sql = "SELECT * FROM `sb_influencer` WHERE status=0 AND view=1";
		if (!$result = $this->DB->query($sql)) {
			$this->DB->error();
			$this->DB->close();
			return false;
		}else{
			$this->DB->close();
			if($result->num_rows>8)
				return true;
			else
				return false;
		}		
	}

	function getInfluencers(){
		$this->DB->open();	
		$cards = "";			
		$sql = "SELECT id, name, text, img_cover, twitter, instagram, url, username FROM `sb_influencer` WHERE status=0 AND view=1 ORDER BY position ASC";
		if (!$result = $this->DB->query($sql)) { 			
    		$this->DB->error();
		}
		$return["url"] = _URL;
		$moreThan8 = false;
		$i=1;
		while($data=$result->fetch_assoc()){
			$return["id"]			= $data["id"];
			$return["name"]			= $data["name"];	

			if(!empty($data["url"])) 
				$return["url_profile"]  = $data["url"];	
			else 
				$return["url_profile"]  = "";	

			$return["text"]			=substr($data["text"],0,250). '...';
			if($data["img_cover"]!=''){
				$return["img_cover"]=_URL."magic/files/".$data["img_cover"];				
			}else{
				$return["img_cover"]=_URL."img/bg_admin.jpg";				
			}
			$return["twitter"]	=$data["twitter"];
			$return["instagram"]=$data["instagram"];			
			$return["username"] =$data["username"];	
			$cards.= $this->RENDER->template($return,"template/index/card_influencer.tpl");
			$i++;
			if($i==9){
				$cards.='<div id="more_influencers">';
				$moreThan8 = true;
			}
		}
		if($moreThan8)
			$cards.='</div>';
		$this->DB->close();
			
		return $cards;
	}

	function addContact(){
		$this->DB->open();				

		$sql = "UPDATE `sb_influencer_register` SET `phone` = '".$_POST['phone']."',
										   `email` = '".$_POST['email']."'
										    WHERE `id_instagram` ='".$_SESSION['info']->data->id."'";
		
		if (!$result = $this->DB->query($sql)) { 			
    		$this->DB->error();
    		$this->DB->close();
    		return false;
		}else{
			$this->DB->close();
			return true;
		}
	}

	function addRegister(){
		$this->DB->open();				

		$sql = "UPDATE `sb_influencer_register` SET `accepted` = '1',
							  						`phone` = '".$_POST['phone']."',
										   			`email` = '".$_POST['email']."'
										    WHERE `id_instagram` ='".$_SESSION['info']->data->id."'";
		
		if (!$result = $this->DB->query($sql)) { 			
    		$this->DB->error();
    		$this->DB->close();
    		return false;
		}else{
			$this->DB->close();
			return true;
		}
	}

	function setData($influencer){
		$this->DB->open();				

		$sql = "UPDATE `sb_influencer` SET `name` = '".$_POST['name']."',
										   `url` = '".$this->getFriendlyURL($_POST['username'])."',
										   `text`='".$_POST['text']."',
										   `img_cover`='".$_POST['img_cover']."',
										   `twitter`='".$_POST['twitter']."',
										   `view`='".$_POST['view']."',
										   `instagram`='".$_POST['instagram']."',
										   `username`='".$_POST['username']."'
										    WHERE `url` ='".$influencer."'";
		
		if (!$result = $this->DB->query($sql)) { 			
    		$this->DB->error();
    		$this->DB->close();
    		return false;
		}else{
			$this->DB->close();
			return true;
		}
	}

	function deleteData($influencer){
		$this->DB->open();							
		$sql = "UPDATE `sb_influencer` SET `status` = '1' WHERE username='".$influencer."'";
		
		if (!$result = $this->DB->query($sql)) { 			
    		$this->DB->error();
    		$this->DB->close();
    		return false;
		}else{
			$this->DB->close();
			return true;
		}
	}

	function addData($post){
		$this->DB->open();				
		
		$sql = "INSERT INTO `sb_influencer` (`name`, `text`,`url`,`img_cover`,`view`,`twitter`,`instagram`,`username`) 
									  VALUE ('".$post['name']."',
										     '".$post['text']."',
										     '".$this->getFriendlyURL($post['username'])."',
										   	 '".$post['img_cover']."',
										   	 '".$post['view']."',
										   	 '".$post['twitter']."',
										     '".$post['instagram']."',
										     '".$post['username']."')";										    
		
		if (!$result = $this->DB->query($sql)) { 			
    		$this->DB->error();
    		$this->DB->close();
    		return false;
		}else{
			$this->DB->close();
			return true;
		}
	}

	function getAction($action,$name){		
		if(($action=='update' || $action=='admin') && $name!='') $return = 'update/'.$name;
		else $return = 'insert/';
		return _URL.$return;
	}

	function printRegister(){
		$this->DB->open();	
		$cards = "";			
		$sql = "SELECT * FROM `sb_influencer_register` ORDER BY created_at";
		if (!$result = $this->DB->query($sql)) { 			
    		$this->DB->error();
		}
		$print = "";
		while($data=$result->fetch_assoc()){
			$return["id"]			= $data["id_instagram"];
			$return["username"]		= $data["username"];	
			$return["fullname"] 	= $data["fullname"];				
			$return["bio"] 			= $data["bio"];
			$return["followed_by"]	= $data["followed_by"];		
			$return["follows"]		= $data["follows"];
			$return["media"]		= $data["media"];			
			$return["profile_picture"] 	= $data["profile_picture"];	
			$return["created_at"] 	= $data["created_at"];
			if($return["followed_by"]<10000){
				$return["accepted"] 	= "";
			}else{
				if($data["accepted"]==1){
					$return["accepted"] 	= "Condiciones aceptadas";
				}else{
					$return["accepted"] 	= "NO ha aceptado las condiciones";
				}
				
			}
			
			$return["phone"] 		= $data["phone"];
			$return["mail"] 		= $data["email"];			
			$return["update_at"] 	= $data["update_at"];
			
			$print .= $this->RENDER->template($return,"template/register/card.tpl"); 
		}		
		$this->DB->close();
		
		return $print;
	}
}
?>