<?php
include_once("config.php");
include_once("class.DB.php");
include_once("class.RENDER.php");
include_once("class.INSTAGRAM.php");
	
class top{	

	var $JSON;
	var $user;
	var $obj;
	var $rawPage;
	var $DB;
	var $INSTAGRAM;
	var $RENDER;
	var $position;

	function top(){
		$this->DB 	  = new DB(_SERVER,_USERNAME,_PASSWORD,_DB);
		$this->RENDER = new render();
		$this->position =  $this->getLastPositionIni();
	}
	
	function addTop($username){
		if($username!=""){
			if($this->addUser($username))
				if($this->addUserProfile($username))
					//return $this->addPosition($username);
					return true;
		}else{
			return false;
		}
	}

	function getLastPositionIni(){
		$this->DB->open();
		if(!$this->isRanked()){
			$position = -1;
		}else{
			$position = $this->getLastPosition();	
		}		
		$this->DB->close();
		return $position;
	}

	function getLastPosition(){
		$sql = "SELECT * FROM `sb_top_position`
				ORDER BY position DESC LIMIT 1";							    

		if (!$result = $this->DB->query($sql)) { 			
    		$this->DB->error();
    		return null;
		}else{
			if($result->num_rows>0){								
				$data=$result->fetch_assoc();
				return $data['position'];
			}else{											
				return 0;
			}			
		}
	}

	function isSclusiber($user){
		$sql = "SELECT * FROM `sb_influencer` 
				WHERE username='$user' AND view=1";							    

		if (!$result = $this->DB->query($sql)) { 			
    		$this->DB->error();
    		return "";
		}else{
			if($result->num_rows>0){								
				$data=$result->fetch_assoc();
				return '<a href="'._URL.'/'.$data['url'].'" class="sclusiber">esta en sclusib!</a>';
			}else{											
				return "";
			}			
		}
	}


	function isNewPosition($id){
		$sql = "SELECT * FROM `sb_top_position`
				WHERE id=$id";							    

		if (!$result = $this->DB->query($sql)) { 			
    		$this->DB->error();
    		return null;
		}else{
			if($result->num_rows>0){	
				echo $id.' ---- '.$result->num_rows." > FALSE";					
				$return = false;
			}else{				
				echo $id.' ---- '.$result->num_rows." > TRUE";												
				$return = true;
			}
		}
	}

	function addPosition($username){
		$this->INSTAGRAM= new instagram($username);
		$this->DB->open();				
		if($this->INSTAGRAM->ok){
			if($this->isRanked()){
				if(!$this->isNewPosition($this->INSTAGRAM->getId())){
					$sql = "INSERT INTO `sb_top_position` ( 
													`id_instagram`,
													`position`,
													`created_at`
													) 
											VALUE (
												  	'".$this->INSTAGRAM->getId()."',
													'".($this->getLastPosition()+1)."',
													CURRENT_TIMESTAMP
													)";										    													
				}
				unset($this->INSTAGRAM);		
				if (!$result = $this->DB->query($sql)) { 			
			    	$this->DB->error();
			    	$this->DB->close();
			    	return false;
				}else{
					$this->DB->close();	
					return true;
				}
			}else{
				return true;
			}
		}else{
			return false;
		}
	}

	function addUser($username){
		$this->INSTAGRAM= new instagram($username);
		$this->DB->open();	
		if($this->INSTAGRAM->ok!="OK")echo "No se ha encontrado ese usuario";			
		if($this->INSTAGRAM->ok){
			if($this->INSTAGRAM->getId()==0){
				echo "No se puede recuperar los datos de este usuario Avisa a FER";
				return false;
			}else{
				if(!$this->INSTAGRAM->isPrivate){				
					$sql = "INSERT INTO `sb_top` ( 
													`id_instagram`,
													`username`,
													`created_at`
													) 
											VALUE (
												  	'".$this->INSTAGRAM->getId()."',
													'".$username."',
													CURRENT_TIMESTAMP
													)";										    
					unset($this->INSTAGRAM);
					
					if (!$result = $this->DB->query($sql)) { 			
			    		$this->DB->error();
			    		$this->DB->close();
			    		return false;
					}else{
						$this->DB->close();	
						return true;
					}			
				}else{
					echo "El usuario parece que es privado";
					return false;
				}
			}
		}else{
			return false;
		}
	}

	function addUserProfile($username){
		$this->INSTAGRAM= new instagram($username);
		$this->DB->open();				
		if($this->INSTAGRAM->ok){
			$sql = "INSERT INTO `sb_user_update` ( 
											`id_instagram`,
											`username`,
											`biography`,
											`followers`,
											`picture`,
											`created_at`) 
									VALUE (
										  	'".$this->INSTAGRAM->getId()."',
											'".$username."',
											'".$this->INSTAGRAM->getBio()."',
											'".$this->INSTAGRAM->getFollowers()."',
											'".$this->INSTAGRAM->getPicture()."',
											CURRENT_TIMESTAMP
											)";									    
			unset($this->INSTAGRAM);

			if (!$result = $this->DB->query($sql)) { 			
	    		$this->DB->error();
	    		$this->DB->close();
	    		return false;
			}else{
				$this->DB->close();	
				return true;
			}
		}else{
			return false;
		}
	}

	function addList(){

		$this->DB->open();			
		if($_POST['newListName']!=""){
			$sql = "INSERT INTO `sb_top_list` ( 											
											`created_at`,
											`list_name`
											) 
									VALUE (
											CURRENT_TIMESTAMP,
											'".$_POST['newListName']."'
											)";		
			if (!$result = $this->DB->query($sql)) { 			
	    		$this->DB->error();
			}		
		}						    
		for($i=0; $i<count($_POST['id']);$i++){
			//echo $_POST['id'][$i].' - '.$_POST['pos'][$i];;
			$sql = "INSERT INTO `sb_top_position` ( 
											`id_instagram`,
											`position`,
											`created_at`,
											`list_name`
											) 
									VALUE (
										  	'".$_POST['id'][$i]."',
											'".$_POST['pos'][$i]."',
											CURRENT_TIMESTAMP,
											'".$_POST['newListName']."'
											)";		
			if (!$result = $this->DB->query($sql)) { 			
	    		$this->DB->error();
			}								    
		}
		$this->DB->close();	
		
		
	}	

	function addDesc(){		
		$this->DB->open();			
		if($_POST['newDesc']!=""){
			$sql = "INSERT INTO `sb_top_desc` ( 	
											`id_instagram`,										
											`created_at`,
											`desc`,
											`pos`
											) 
									VALUE (
											'".$_POST['idDesc']."',
											CURRENT_TIMESTAMP,
											'".$_POST['newDesc']."',
											'".$_POST['posDesc']."'
											)";		
			if (!$result = $this->DB->query($sql)) { 			
	    		$this->DB->error();
			}		
		}	
		$this->DB->close();		
	}

	function getDesc($id){
		$sql = "SELECT * FROM sb_top_desc
				WHERE id_instagram = $id				
				ORDER BY created_at DESC LIMIT 1";							    
		
		if (!$result = $this->DB->query($sql)) { 			
    		$this->DB->error();
    		return '';
		}else{			
			if($result->num_rows>0){				
				$data	= $result->fetch_assoc();
				$return = $data['desc'];
			}else{											
				$return = '';
			}
			//echo ">>>>".$id." - ".$result->num_rows." - ".$return;
			return $return;
		}
	}

	function isRanked(){
		$sql = "SELECT * FROM sb_top_position				
				ORDER BY created_at DESC LIMIT 1";							    
		
		if (!$result = $this->DB->query($sql)) { 			
    		$this->DB->error();
    		return null;
		}else{			
			if($result->num_rows>0){				
				$return = true;
			}else{											
				$return = false;
			}
			return $return;
		}
	}

	function getPosition($id){
		$sql = "SELECT * FROM sb_top_position
				WHERE 
				id_instagram = $id 
				ORDER BY created_at DESC LIMIT 1";							    
		
		if (!$result = $this->DB->query($sql)) { 			
    		$this->DB->error();
    		return null;
		}else{			
			if($result->num_rows>0){
				$data	= $result->fetch_assoc();
				$return = $data['position'];				
				//echo ">>>>>".$id." - ".$return;
			}else{								
				//echo $this->position;
				$this->position++;		
				$return = $this->position;
				//echo "<<<<<<".$id." - ".$return;
				
				
			}

			return $return;
		}
	}

	function getStatusPosition($isAdmin,$id){
		$sql = "SELECT * FROM sb_top_position B
				WHERE 
				B.id_instagram = $id 
				ORDER BY B.created_at DESC LIMIT 2
				";	
		if (!$result = $this->DB->query($sql)) { 			
    		$this->DB->error();
    		return "ERROR";
		}else{
			if($isAdmin){
				$NEW 	= "glyphicon-exclamation-sign color_blue";
				$SAME 	= "glyphicon-minus color_grey";
				$UP 	= "glyphicon-triangle-top color_green";
				$DOWN 	= "glyphicon-triangle-bottom color_red";
			}else{
				$NEW 	= "new.png";
				$SAME 	= "same.png";
				$UP 	= "up.png";
				$DOWN 	= "down.png";
			}
			if($result->num_rows==0){
				//Si no existe indice de posiciones todos son nuevos
				return $NEW; 
			}else if($result->num_rows==1){
				//Si solo tiene un registro es nuevo
				return $NEW;
			}else{
				$data_old=$result->fetch_assoc();					
				$data_new=$result->fetch_assoc();					
				if($data_new['position']>$data_old['position']){
					return $UP;	
				}else if($data_new['position']<$data_old['position']){
					return $DOWN;	
				}else{
					return $SAME;		
				}				
			}			
		}	
	}

	function getProfile($id){		
		$sql = "SELECT * FROM sb_user_update B
				WHERE 
				B.id_instagram = $id 
				ORDER BY B.created_at DESC LIMIT 1
				";							    
		
		if (!$result = $this->DB->query($sql)) { 			
    		$this->DB->error();
    		return 0;
		}else{
			$data=$result->fetch_assoc();
			
			
			return $data;	
		}
	}

	function getList(){
		$this->DB->open();	
		$sql = "SELECT * FROM sb_top_position B				
				ORDER BY B.created_at DESC LIMIT 1
				";							    
		
		if (!$result = $this->DB->query($sql)) { 			
    		$this->DB->error();
    		$this->DB->close();	
    		return "";
		}else{
			$data=$result->fetch_assoc();			
			$this->DB->close();						
			return $data['list_name'];	
		}
	}

	function getTop($isAdmin){		
		$this->DB->open();						
		$sql = "SELECT * FROM sb_top A
				WHERE 
				A.isOK=1 ORDER BY created_at ASC";							    
		if($isAdmin)
			$template_card = "template/top/card_top.tpl";
		else 
			$template_card = "template/top/card_top_main.tpl";

		if (!$result = $this->DB->query($sql)) { 			
    		$this->DB->error();
    		return 0;
		}else{
			$template="";
			$i=0;
			while($data=$result->fetch_assoc()){				
				$return["id_instagram"]	= $data["id_instagram"];
				$profile				= $this->getProfile($return['id_instagram']);				
				$return["username"]		= $profile["username"];
				$return["img_profile"]  = $profile["picture"];	
				$return["isSclusiber"]  = $this->isSclusiber($profile["username"]);
				$return["followers"]	= number_format($profile["followers"],0,',','.');	
				$return["position"]		= $this->getPosition($profile["id_instagram"]);
				$return["pos"]			= $return["position"];
				$return["pos_number"]	= $return["position"]+1;
				$return["desc"]			= $this->getDesc($return['id_instagram']);				
				$return["status"]		= $this->getStatusPosition($isAdmin,$profile["id_instagram"]);
				$return["url"]			= _URL;
				$cards[$return["position"]]= $return;
					$i++;
			}
			for($c=0;$c<$i;$c++){
				if(!$isAdmin){								
					if($c % 10 == 0 && $c!=0){					
						$template.=$this->RENDER->template($cards[$c],"template/top/card_top_ad.tpl");					
					}
				}	
				$template.=$this->RENDER->template($cards[$c],$template_card);
				
			}			
			$this->DB->close();
			return $template;
		}
	}
	function deleteUser($id){
		
		$this->DB->open();
		$sql = "UPDATE sb_top					
				SET isOK = 0		
				WHERE 
				id_instagram = $id";
		
		if (!$result = $this->DB->query($sql)) { 			
    		$this->DB->error();
    		$this->DB->close();
    		return false;
		}else{					
			
			$this->reorderTopFrom($id);
			$this->DB->close();	
			return true;
		}
	}

	function updatePosition($id,$new_position){		
		$sql = "UPDATE sb_top_position		
				SET position = $new_position
				WHERE 
				id = $id";
		
		if (!$result = $this->DB->query($sql)) { 			
    		$this->DB->error();
    		return false;
		}else{											
			return true;
		}
	}

	function cronInstagram($offset){
		$this->DB->open();						
		$sql = "SELECT * FROM sb_top A
				WHERE 
				A.isOK=1 ORDER BY created_at ASC LIMIT 10 OFFSET ".$offset;
		if (!$result = $this->DB->query($sql)) { 			
    		$this->DB->error();
    		//echo "ERROR";
		}else{		
			$i=0;	
			$e=0;
			while($data=$result->fetch_assoc()){				
				if($this->cronInsert($data['username'])){
					$i++;
				}else{
					$e++;
				}
			}

		}
		echo "Correctamente actualizados: ". $i .", ERRORES: ". $e;
		$this->DB->close();
		return $offset+10;
	}

	function cronInsert($username){
		$this->INSTAGRAM= new instagram($username);
			if($this->INSTAGRAM->ok){
				if($this->INSTAGRAM->getId()!=null || $this->INSTAGRAM->getId()!=0){

				$sql = "INSERT INTO `sb_user_update` ( 
										`id_instagram`,
										`username`,
										`biography`,
										`followers`,
										`picture`,
										`created_at`) 
								VALUE (
									  	'".$this->INSTAGRAM->getId()."',
										'".$this->INSTAGRAM->user."',
										'".$this->INSTAGRAM->getBio()."',
										'".$this->INSTAGRAM->getFollowers()."',
										'".$this->INSTAGRAM->getPicture()."',
										CURRENT_TIMESTAMP
										)";										    
		
					if (!$result = $this->DB->query($sql)) { 			
			    		$this->DB->error();
			    		$this->DB->close();
			    		return false;
					}else{			
						echo "INSERTADO: ".$this->INSTAGRAM->user;
						return true;
					}
				}else{
					echo "ERROR INSTAGRAM ID: ".$username;
					return false;
				}
			}else{
				echo "ERROR INSTAGRAM JSON: ".$username;
				return false;
			}
	}

	function reorderTopFrom($id){

		if($this->getPosition($id)!=0){			
			$sql = "SELECT * FROM sb_top_position
					WHERE 
					id_instagram = $id 
					ORDER BY created_at DESC LIMIT 1
					";							    
			
			if (!$result = $this->DB->query($sql)) { 			
	    		$this->DB->error();
	    		return 0;
			}else{
				$data=$result->fetch_assoc();	
				//ELIMINAMOS EL REGISTRO
				$id_delete = $data['id'];
				$position 	   = $data['position']+1;
				$back_position = $position-1;

				$sql = "DELETE FROM sb_top_position
						WHERE id = $id_delete";
				if (!$result = $this->DB->query($sql)) { 			
			    		$this->DB->error();			
				}

				//ACTUALIZAMOS LOS SIGUIENTES	
				for($i=$data['position'];$i<$this->getLastPosition();$i++){

					$sql = "SELECT * FROM sb_top_position
					WHERE 
					position = $position 
					ORDER BY created_at DESC LIMIT 1
					";						
					
					if (!$result = $this->DB->query($sql)) { 			
			    		$this->DB->error();
					}else{
						$next=$result->fetch_assoc();
						$this->updatePosition($next['id'],$back_position);
						$position++;
						$back_position = $position-1;
					}
				}								
				return $data;	
			}			
		}
	}
}
?>