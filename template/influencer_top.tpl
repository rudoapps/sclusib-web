{head}
<body>
<div class="header color_back">	
	<div class="container">
		{menu}
	</div>
</div>
<div class="main_section color_back">
	<div class="container">						     
		<div class="row">		        
		    <div class="col-md-12 center">	
		    	<br/><br/>	    	
		    	<img src="{url}img/influencer/top/WOW.png" class="influencer_responsive_small_img">
		    	<p class="influencer_responsive_small">TIENES UN MONTÓN DE SEGUIDORES ¡ENHORABUENA!</p>
		    	<h2 class="influencer_responsive_main">TIENES UN MONTÓN DE SEGUIDORES ¡ENHORABUENA!</h2>
		    </div>
		</div>
	</div>
	
	<div class="container">
		<div class="influencer_top_wrap">					      
			<div class="row">		        
			    <div class="col-md-6">			    
			    	<img src="{url}img/influencer/top/ganar_pasta.jpg" class="influencer_top_img_left">
			    </div>
			    <div class="col-md-6">			    	
			        <div class="influencer_top_text influencer_top_block1">	
			        						        		
				        <p>
				        Imaginamos que ya estás monetizando todo ese esfuerzo o que estás pensando en hacerlo. Ya tienes claro que puedes ganar dinero con:
				        </p>
				        <br/>
				        <div class="row">		        
			    			<div class="col-xs-2"><img src="{url}img/influencer/top/Las_marcas.png"></div>			    			
			    			<div class="col-xs-10"><b>Las marcas.</b> Te dan regalos o dinero por hacerles publicidad.</div>				        
				        </div>
				        <br/>				        
				        <div class="row">		        
			    			<div class="col-xs-2"><img src="{url}img/influencer/top/Youtube.png"></div>			    			
			    			<div class="col-xs-10"><b>Youtube.</b> Si tienes canal y eres partner, te pagan por cada visualización.</div>				        
				        </div>
				        <br/>
				        <div class="row">		        
			    			<div class="col-xs-2"> <img src="{url}img/influencer/top/Tus_fotos.png"></div>			    			
			    			<div class="col-xs-10"><b>¿Tus fotos?.</b> Estás viendo que Instagram, Snapchat, Facebook o Twitter se forran a tu costa y no comparten nada contigo</div>				        
				        </div>
						<br/>
				        <p>
				        Nosotros te pagamos por subir fotos en <b>nuestra aplicación</b>. Cuanta más gente las vea más dinero ganas.
				        </p>				            
			        </div>						    
			    </div>
			</div>			
		</div>



		<div class="space"></div>

		<div class="influencer_top_wrap">					      
			<div class="row">		        
			    <div class="col-md-6">
			    	<div class="influencer_top_text influencer_top_block2">	
			        	<b>¿Qué es sclusib?.</b>	<br/> <br/>        					        		
				        <p>
				        Sclusib es una aplicación para android y para iOS que funciona de manera similar a Youtube
				        </p>
				       	<div class="row">		        
			    			<div class="col-xs-5 center">
			    				<img src="{url}img/influencer/top/youtube_ilustracion.png">
			    				<ul class="influencer_top_list">
			    				<li>Te hace un canal</li>
			    				<li>Subes un video</li>
			    				<li>Ponen publicidad en tu video</li>
			    				<li>Te pagan por cada visualización</li>
			    				</ul>
			    			</div>			    			
			    			<div class="col-xs-2">

			    			</div>		    			
			    			<div class="col-xs-5 center">
			    				<img src="{url}img/influencer/top/Sclusib_ilustracion.png"><br/>
			    				<ul  class="influencer_top_list">
			    				<li>Te haces un perfil</li>
			    				<li>Subes una foto</li>
			    				<li>Ponemos publicidad cerca de tu foto</li>
			    				<li>Te pagamos en función de la gente que lo vea.</li>
			    				</ul>
			    			</div>				        
				        </div>	
				        <br/> 				    
					    <p>
					    En definitiva <b>te pagamos por subir fotos en nuestra aplicación</b>
					    </p>				        
			        </div>						    
			    </div>
			    
			    <div class="col-md-6">	
			   		<img src="{url}img/influencer/top/in_sclusib_we_trust.jpg" class="influencer_top_img_right" align="right">		    	
			    </div>   
			</div>			
		</div>

		<div class="space"></div>

		<div class="influencer_top_wrap">					      
			<div class="row">		        
			    <div class="col-md-6">
			    	<img src="{url}img/influencer/top/Que_tengo_que_hacer.jpg" class="influencer_top_img_left">
			    </div>
			    <div class="col-md-6">			    	
			        <div class="influencer_top_text influencer_top_block3">	
			        	<b>Vale, me gusta. ¿Qué tengo que hacer?</b><br/><br/> 	        					        		
				        <p>
				        Bájate la app. Por ahora tienes el perfil de "fan". En sclusib sólo pueden subir fotos los influencers verificados.				      
				        Déjanos tus datos y acepta el <a href="acuerdo-de-partner" target="_blank">acuerdo de partner</a> para que te demos de alta y puedas subir fotos. Te avisaremos cuando lo hayamos verificado. 
				        </p>
				        </br>
				        <form method="post" id="top_influencer" action="{url}unete/inscribirme">   

				        				        		
							 
				        	
			        		<div class="influencer_top_button">		
			        			<input type="text" class="telefono" name="phone"  placeholder="Escribe aquí tu número de teléfono..." required>
			        			<input type="text" class="email" name="email" placeholder="Escribe aquí tu número de correo..." required> 
						    	<input type="checkbox" name="accepted" id="top_influencer_check" style="float:left;"> Acepto el <a href="acuerdo-de-partner" target="_blank">acuerdo de partner</a>
						    	<button class="button large always middle top_influencer">Inscribirme</button>
						   	</div>

						</form>
						
						        <div class="influencer_top_more">			      
						        	<br/><b>¿Tienes más dudas?</b><br/>
						        	Puedes acceder a nuestra sección de <a href="preguntas-frecuentes" target="_blank">preguntas frecuentes</a> desde aquí.<br/></br>
						        	Tambien puedes enviarnos un DM por instagram o un mail a <a href="mailto:hola@sclusib.com">hola@sclusib.com</a>			        	
						        </div>						    
						
			        </div>						    
			    </div>
			</div>			
		</div>

		<div class="space"></div>
	</div>


</div>
{footer}
		
</body>
</html>