<h1>Acuerdo partner sclusib</h1>
 
<h2>¿Qué es sclusib?</h2>
<p>
Sclusib es una aplicación de “famosos” para fans, cualquiera puede utilizarla, pero solo pueden subir fotos las personas con las que hayamos firmado un acuerdo de partner. Los fans podrán comentar sus fotos, compartirlas o decir que les gustan, pero no podrán subir contenido.
 </p>
 <p>
En calidad de partner de sclusib tendrás un acceso exclusivo a la plataforma (diferente del que tienen los fans) con el que podrás subir fotos y monetizarlas a través de la publicidad que colocamos en la aplicación.
 </p>
 <h2>¿Cómo funciona sclusib?</h2>
 <ol>
<li>Sube contenido: una vez hayas firmado el acuerdo como partner y te hayamos verificado sube algunas fotos para que tu perfil no esté vacío cuando te visitan por primera vez.</li>
<li>Consigue seguidores: anima a tus seguidores en otras redes sociales a que te sigan también en sclusib, ya que cuantos más seguidores, más visualizaciones y más clicks en los anuncios y por tanto, más dinero para ti.</li>
<li>Interactúa con tus fans: contesta a sus comentarios, da like a los que más te gusten, ofrece premios a los fans de la semana y del mes… Cuando más interactúas, más engagement consigues y por tanto tus seguidores volverán cada día a la app, verán más anuncios y más dinero para ti. </li>
</ol>

<h2>
¿Qué deberes tengo como partner?
</h2>
<ul>
<li>Dedicar al menos 5 minutos al día a sclusib. Lo justo para subir un par de fotos e interactuar con tus seguidores (respondiendo y dando like a alguno de sus comentarios).</li>
<li>Dar los premios que has prometido. Para un fan lo mejor de sclusib es estar más cerca de ti. Se preocupan mucho por interactuar y conseguir puntos para ser TU fan número uno. Así que necesitamos que cada semana propongas un premio y lo des (los premios pueden ser mencionarle en un video, en twitter o en instagram; seguirle en twitter o instagram, dedicarle una foto... )</li>
<li>Subir fotos que no hayas subido antes a otras redes. De esta forma tus fans tendrán un aliciente para  seguirte en otra red y ver publicidad.</li>
</ul>
<p>
Sclusib solo tiene sentido si subes fotos e interactúas con tus fans. Si tu cuenta está totalmente inactiva durante 10 días podremos anularla.
 </p>

<h2>Contenidos de las fotos</h2>
<p>La mayoría de nuestros usuarios son menores por lo que no están permitidas las fotos o comentarios que incluyan:</p>
<ul>
<li>Desnudos o contenido sexualmente explícito o muy sugerente.</li>
<li>Abuso o incitación al odio.</li>
<li>Irreverencia excesiva o violencia gráfica.</li>
<li>Violencia hacia usuarios de la plataforma. </li>
</ul>
  
<h2>¿Cuánto dinero puedo ganar?</h2>
<p>Los ingresos generados dependen principalmente de dos factores.</p>
<ul>
<li>Del número de visualizaciones de publicidad: estas dependen sobre todo del número de seguidores que tienes y del número de fotos que subas.</li>
<li>Del tipo de anuncios que aparecen y del precio al que se venden estos anuncios, esto no lo puedes controlar tu, depende de Google que es nuestro proveedor publicitario.</li>
 </ul>
 <h2>
¿Qué tipo de anuncios aparecerán?</h2>
<p>
Nuestro plataforma publicitaria para anuncios es Google AdMob. Ellos decidirán el tipo y el formato de anuncios que colocarán. Sclusib no estará obligado a siempre poner anuncios junto a tus contenidos, dependerá de la disponibilidad en Google AdMob.</p>
 <h2>
¿Qué porcentaje de los ingresos publicitarios voy a recibir?</h2>
<p>Sclusib te pagará el 50% de los ingresos reconocidos por Google Admob por los anuncios mostrados. Cuantos más seguidores tengas y más fotos subas más dinero ganarás. </p>
 <h2>
¿Cómo recibiré los pagos?</h2>
<p>
Sclusib te pagará a los 60 días aproximadamente tras la finalización de cualquier mes natural (enero, febrero…).</p>
 <p>
No tendrás derecho a ganar o recibir ningún ingreso en relación con tu contenido si has utilizado imágenes sobre los que no tienes los derechos o si tienes actividad de clics no válidos.</p>
 <h2>
¿Qué son los clics no válidos?</h2>
<p>
La actividad de clics no válidos tiene lugar cuando se producen clics e impresiones que pueden aumentar de manera artificial los costes de un anunciante o los ingresos de un partner. Esta actividad incluye, entre otros, los siguientes casos: clics o impresiones generados por alguien que hace clic en sus propios anuncios, un partner que anima a hacer clic en sus anuncios, herramientas de clics y fuentes de tráfico automatizadas, robots y cualquier software destinado al uso fraudulento. Ten en cuenta que los clics en los anuncios deben proceder de usuarios interesados, por lo que cualquier método que genere clics o impresiones de forma artificial queda prohibido terminantemente según las políticas del programa.</p>
 <h2>
Acuerdo NO exclusivo</h2>
<p>
Sclusib es un nuevo canal de monetización compatible con cualquiera que ya tengas (youtube, otros partners MCM, managers, relación con marcas…) o quieras tener en el futuro. No ponemos ninguna restricción en cuanto a las formas en las que puedes ganar dinero. </p>
<p>
Del mismo modo no te pedimos que dejes de usar otras redes sociales, todo lo contrario, sigue usando tus otras redes de la forma habitual pero en sclusib publica fotos nuevas.</p>
<h2>
Vigencia</h2> 
<p>Este acuerdo tiene un carácter indefinido pero se puede dar por finalizado en cualquier momento por ambas partes.  </p>
<ul>
<li>Si no quieres seguir usando sclusib simplemente escríbenos y anularemos tu cuenta pagándote lo que hayas generado según los términos de pago antes establecidos.</li>
<li>Sclusib puede anular tu cuenta si no cumples este acuerdo, debiendo notificarlo por escrito.</li>
</ul>
