{head}
<body>
<div class="header_static">	
	<div class="container">
		{menu}
	</div>
</div>
<div class="main_section color_front">	
	<div class="container">								
				<div class="space"></div>	
	</div>
</div>
<div class="main_section color_front relative">
	<div class="container p40 all-influencers-line">													
		<div class="row">
			<div class="col-md-5 influencerImage">
			    <img src="{url_img_cover}">
			</div>
			<div class="col-md-7">
			    <h1 class="influencerName">{username}</h1>
			    <div class="pop-text">
			    <p>
			    	{text}
			    </p>			    	
			    </div>
			    <div class="space"></div>
			    <div class="row">
			    	<div class="col-md-6 col-xs-12 influencerSocial">
						<a href="https://www.instagram.com/{instagram}/"><img src="{url}img/social/ic_instagram.png" width="30px"></a>						
						<a href="https://twitter.com/{twitter}"><img src="{url}img/social/ic_twitter.png" width="30px"></a>
			    	</div>
			    	<div class="col-md-6 col-xs-12 influencerDownload">
						<a href="{_url_android}" onclick="trackOutboundLink('{_url_android}'); return false;" target="_blank"><img src="{url}img/download-android.png" width="100"></a>		        		
			        	<a href="{_url_ios}" onclick="trackOutboundLink('{_url_ios}'); return false;" target="_blank"><img src="{url}img/download-ios.png" width="100"></a>
			    	</div>
			    </div>
			</div>
		</div>
    </div>   
</div>  
{footer}
		
</body>
</html>