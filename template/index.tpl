{head}
<body>
<div class="header color_back">	
	<div class="container">
		{menu}
	</div>
</div>
<div class="main_section color_back">	
	<div class="container">																
		       <div class="space"></div>		        
		        <div class="row">
		        	
		        	<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-lg-push-6 col-md-push-6">
		        		<div id="main_intro">
			        		<h1>¿Eres el fan número 1?</h1>
			        		<p class="roman sizeMain">Sigue a tus instagramers, tuitstar y youtubers favoritos y demuestra lo que te gustan. Gana puntos para escalar en su ranking Top Fan y podrás ganar premios relacionados con ellos.</p>
			        		<div class="space"></div>
			        		<div class="row">
			        			<div class="col-xs-6 col-sm-6 mainDownload">
			        				<a href="{_url_android}" onclick="trackOutboundLink('{_url_android}'); return false;" target="_blank"><img src="{url}img/download-android.png" width="180"></a>
			        			</div>
			        			<div class="col-xs-6 col-sm-6 mainDownload">
			        				<a href="{_url_ios}" onclick="trackOutboundLink('{_url_ios}'); return false;" target="_blank" ><img src="{url}img/download-ios.png" width="180"></a>
			        			</div>
			        		</div>
		        		</div>
		        	</div>
		        	<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-lg-pull-6 col-md-pull-6">
		        		<center><img src="{url}img/sclusib-mockup.png"></center>
		        	</div>
		        </div>	
		        <div class="row">
		        	<div class="col-md-12 center">
		        		<div class="space"></div>
		        		<img src="{url}img/ic_bajar.png">		        		
		        		<div class="space"></div>
		        	</div>
		        </div>
	</div>
</div>	
<div class="main_section color_front">
	<div class="container">
		<div class="row">			
			<div class="col-md-12 center">
				<h2>¿CÓMO FUNCIONA?</h2>				
			</div>
		</div>
		<div class="row">
			<div class="col-xs-6 col-sm-3 center">
				<div class="box-image"><img src="{url}img/ic_unirse.png"></div>
				<p class="box-text roman">Bájate <br/>nuestra APP</p>
			</div>
			<div class="col-xs-6 col-sm-3 center">
				<div class="box-image"><img src="{url}img/ic_visualiza.png"></div>
				<p class="box-text roman">Visualiza contenido<br/> exclusivo</p>
			</div>
			<div class="col-xs-6 col-sm-3 center">
				<div class="box-image"><img src="{url}img/ic_posiciona.png"></div>
				<p class="box-text roman">Sé el primero en<br/> nuestro ranking</p>
			</div>
			<div class="col-xs-6 col-sm-3 center">
				<div class="box-image"><img src="{url}img/ic_premio.png"></div>
				<p class="box-text roman">Recibe premios por<br/> parte de tus ídolos</p>
			</div>			
		</div>			
		<div class="space p40"></div>
	</div>
</div>	
<div class="main_section color_back">
	<div class="container">
		<div class="row">			
			<div class="col-md-12 center">
				<h2 id="talentos">YA ESTÁN EN SCLUSIB</h2>				
			</div>
		</div>
		<div class="row">
			{influencers}
		</div>
		<div class="row">
			<div class="col-md-12 center">
				{see_all}
			</div>
		</div>
		<div class="space p40"></div>
	</div>
</div>	
<div class="main_section color_front">
	<div class="container">
		<div class="row">			
			<div class="col-md-12 center">
				<h2>¡DESCÁRGATELA AHORA! :)</h2>				
			</div>
		</div>
		<div class="row">
			<div class="col-xs-6 col-sm-6 downloadLeft">
			    <a href="{_url_android}" onclick="trackOutboundLink('{_url_android}'); return false;" target="_blank"><img src="{url}img/download-android.png" width="180"></a>
			</div>
			<div class="col-xs-6 col-sm-6 downloadRight">
				<a href="{_url_ios}" onclick="trackOutboundLink('{_url_ios}'); return false;" target="_blank"><img src="{url}img/download-ios.png" width="180"></a>
			</div>
		</div>
		<div class="space p40">
		</div>
	</div>
</div>
<div class="main_section bg">
	<div class="container bg-line">
		<div class="row">
			<div class="col-md-12 center">
				<h2>EXPERIENCIAS</h2>
				
				<div id="cycler">					
					<img class="active" src="{url}img/testimonio/mockup1.png" alt="" />
					<img src="{url}img/testimonio/mockup2.png" alt="" />						
				</div>
				<!--
				<div id="cycler">
					<img src="{url}img/testimonio/mockup_user.png" class="mask-mobile">
					<img class="active" src="{url}img/testimonio/1.png" alt="My image" />
					<img src="{url}img/testimonio/2.png" alt="My image" />						
				</div>-->
				
				<!--<img src="{url}img/testimonio/1.png" class="mask-testimony">		-->
			</div>
		</div>
		<div class="space">
		</div>
		<div class="row">
			<div class="col-xs-6 col-sm-6 downloadLeft">
			    <a href="{_url_android}" onclick="trackOutboundLink('{_url_android}'); return false;" target="_blank"><img src="{url}img/download-android.png" width="180"></a>
			</div>
			<div class="col-xs-6 col-sm-6 downloadRight">
				<a href="{_url_ios}" onclick="trackOutboundLink('{_url_ios}'); return false;" target="_blank"><img src="{url}img/download-ios.png" width="180"></a>
			</div>
		</div>
		<div class="space p40">
		</div>
	</div>
</div>
{footer}
		
</body>
</html>