{head}
<body>
<div class="header_static">	
	<div class="container">
		{menu}
	</div>
</div>
<div class="main_section color_front">	
	<div class="container">								
		<div class="space"></div>	
	</div>
</div>
<div class="main_section color_front">
	<div class="container">	
		<div class="space"></div>	
		<div id="error404">
													
		<div class="row">					
			<div class="col-md-12">
			<h1>No tienes permisos para acceder</h1>
			<br/>
			<p>Pero te voy a dar alguna sugerencia... ¿Qué te parece ver nuestro <b><a href="{url}/top/">TOP100 de Instagramers</a></b>?<br/> o mejor todavía <b>descárgate nuestra APP</b> y mantente al día de todas las novedades</p>			
			</div>									
						
		</div>	
		<div class="row">
		        	<div class="col-md-12 center">
		        		<div class="space"></div>
		        		<img src="{url}img/ic_bajar.png">		        		
		        		<div class="space"></div>
		        	</div>
		        </div	
		<div class="row">
			<div class="col-xs-6 col-sm-6 downloadLeft">
			    <a href="{_url_android}" onclick="trackOutboundLink('{_url_android}'); return false;" target="_blank"><img src="{url}img/download-android.png" width="180"></a>
			</div>
			<div class="col-xs-6 col-sm-6 downloadRight">
				<a href="{_url_ios}" onclick="trackOutboundLink('{_url_ios}'); return false;" target="_blank"><img src="{url}img/download-ios.png" width="180"></a>
			</div>
		</div>

		</div>											
	</div>
</div>  
{footer}		
</body>
</html>