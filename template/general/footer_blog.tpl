<div style="background-color:#414042; color:#FFF; padding:40px 0;">
	<div class="container">
		<div class="space p20"></div>
		<div class="row">
			<div style="float:left; text-align:left; padding-left:20px">
				<img src="http://www.sclusib.com/img/logo-sclusib-inverted.png"><br/>
			    <div class="footerAddress"><span style="font-size:10px;">Sclusib Studio S.L. <br/>Calle Roger de Lauria 19 Valencia (Spain)</span></div>			
	    	</div>
	    	
				<a href="https://www.instagram.com/sclusib/"><img src="{url}img/social/instagram.png" align="right"></a>
				<a href="https://twitter.com/sclusib"><img src="{url}img/social/twitter.png" align="right"></a>				
	    	
	    </div>
	    <div style="border-bottom: 1px solid #FFF; margin-top: 10px; margin-bottom:10px;"></div>
		<div class="row color_white roman size15">			
			<div style="float:left; text-align:left; padding-left:20px;">
			    Consigue estar más cerca de tús ídolos.<br/>
			    ¡Ve contenido y gana premios!
			</div>
			<div style="float:left; text-align:right;">			    
			<!--			    			   
			    <div class="row">			    			    				    
			    	<div class="col-md-6 col-xs-6">
			    		<ul class="nav-footer">
			    			<li><a href="#">Preguntas frecuentes</a></li>
			    			<li><a href="#">Nuestros talentos</a></li>
			    			<li><a href="#">Ayuda</a></li>
			    			<li><a href="#">Acerca de sclusib</a></li>
			    		</ul>
			    	</div>
			    	<div class="col-md-6 col-xs-6">
			    		<ul class="nav-footer">
			    			<li><a href="#">Contacto</a></li>
			    			<li><a href="#">Cookies</a></li>
			    			<li><a href="#">Política de Privacidad</a></li>
			    			<li><a href="#">Términos de uso</a></li>
			    		</ul>
			    	</div>			    				    				   
			    </div>-->
			</div>			
			<div style="clear:both;"></div>
		</div>
		<div style="padding-bottom:30px;"></div>			
	</div>

	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-61907046-1', 'auto');
	  ga('send', 'pageview');

	</script>		
</div>