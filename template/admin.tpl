{head}
<body>
{menu_admin}
<div class="main_section color_front">	
	<div class="container">				
								
				<div class="space"></div>
				
				<div class="row">
						        	<div class="col-md-2">
						        		
						        	</div>
						        	<div class="col-md-10 right">
				
											<select name="influencer" class="admin-select">
							        			{select}
							        		</select>
						        			<button type="button" class="btn btn-default  btn-sm" id="add">
											 	<span class="glyphicon glyphicon-plus-sign" aria-hidden="true"></span> 
											</button>		
						        	</div>	        	
						        </div>
	</div>
</div>
<form method="POST" action="{action}" id="profile">
<div class="main_section color_front relative">
	<div class="container p40 all-influencers-line">													
		<div class="row">
			<div class="col-md-5 left">
			    <div class="upload-img">
			    	318 x 551
			    	<img src="{url_img_cover}" id="cover">
			    </div>	
			    <br/>
			    	<span class="btn btn-success fileinput-button">
					    <span>Elegir</span>
					    <!-- The file input field used as target for the file upload widget -->
					    <input id="fileupload" type="file" name="files[]" multiple>
					  </span>
					  <br/>
					  <br/>
					  
					  <!-- The global progress bar -->					  
					  <div id="progress" class="progress progress-success progress-striped">
					    <div class="bar"></div>
					  </div>
					  					  					  									  
					  <ul id="files"></ul>					  					  
					  <input type="hidden" name="img_cover" value="{img_cover}" id="img_cover">
					  <!-- Load jQuery and the necessary widget JS files to enable file upload -->
					  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
					  <script src="{url}js/jquery.ui.widget.js"></script>
					  <script src="{url}js/jquery.iframe-transport.js"></script>
					  <script src="{url}js/jquery.fileupload.js"></script>		   
			</div>
			<div class="col-md-7">
				<div class="row">
					<div class="col-md-2">
						Visible: 
					</div>
					<div class="col-md-2">
						<select name="view">
						{view}			
						</select>
					</div>
					<div class="col-md-2">
						Nombre real:
					</div>
					<div class="col-md-6">
					 	<input type="text" name="name" value="{name}">
					</div>
				</div>
				
				<input type="text" name="username" class="admin-input h1" size="13" maxlength="16" value="{username}">			    
			    <div class="pop-text">
			    	<textarea class="admin-textarea" name="text">{text}</textarea>			    	
			    </div>
			    <div class="space"></div>
			    <div class="row">
			    	<div class="col-md-6 right">
						<img src="{url}img/social/ic_twitter.png" width="30px">												
			    	</div>
			    	<div class="col-md-6 left">						
			        	<input type="text" name="twitter" class="admin-input" value="{twitter}">
			    	</div>
			    </div>
			    <br/>
			    <div class="row">
			    	<div class="col-md-6 right">
						<img src="{url}img/social/ic_instagram.png" width="30px">												
			    	</div>
			    	<div class="col-md-6 left">
						<input type="text" name="instagram" class="admin-input" value="{instagram}">			        					        	
			    	</div>
			    </div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">			
				Palabras clave:<br/>			
				<textarea class="admin-textarea-key" name="keywords"></textarea>
			</div>
		</div>
    </div>   
</div> 

</form>
<center>

	<button type="button" class="btn btn-danger  btn-sm" id="delete">
		BORRAR    <span class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span> 
	</button>
	
	<button type="button" class="btn btn-default  btn-sm" id="preview">
		PREVIEW    <span class="glyphicon glyphicon-camera" aria-hidden="true"></span> 
	</button>
	
	<button type="button" class="btn btn-success  btn-sm" id="accept">
		ACEPTAR    <span class="glyphicon glyphicon-send" aria-hidden="true"></span> 
	</button>	
	<br/>
	<br/>
	
</center>
<script>
    // When the server is ready...
    $(function () {
        'use strict';
        
        // Define the url to send the image data to
        var url = '{url-website}magic/files.php';
        
        // Call the fileupload widget and set some parameters
        $('#fileupload').fileupload({
            url: url,
            dataType: 'json',
            done: function (e, data) {
                // Add each uploaded file name to the #files list
                $.each(data.result.files, function (index, file) {
                    $('<li/>').text(file.name).appendTo('#files');
                    $('#img_cover').val(file.name);
                    $('#cover').attr('src', '{url-website}magic/files/'+file.name);                   
                	//alert("asdfasdf"+$('#img_cover').val());
                });

            },
            progressall: function (e, data) {
                // Update the progress bar while files are being uploaded
                var progress = parseInt(data.loaded / data.total * 100, 10);
                $('#progress .bar').css(
                    'width',
                    progress + '%'
                );                
            }
        });
    });
    
  </script>
</body>
</html>