{head}
<body>
<div class="header">	
	<div class="container">
		{menu}
	</div>
</div>
<div class="main_section color_front">	
	<div class="container">								
		<div class="space"></div>	
	</div>
</div>
<div class="main_section color_front">
	<div class="container">	
		<div class="space"></div>															
			<div class="row">					
				<div class="col-md-12 main_documents">
					{content}
				</div>														
			</div>	
		<div class="row">
		    <div class="col-md-12 center">
		    	<div class="space"></div>
		    	<img src="{url}img/ic_bajar.png">		        		
		    	<div class="space"></div>
		   	</div>
	    </div>
		<div class="row">
			<div class="col-xs-6 col-sm-6 downloadLeft">
			    <a href="{_url_android}"><img src="{url}img/download-android.png" width="180"></a>
			</div>
			<div class="col-xs-6 col-sm-6 downloadRight">
				<a href="{_url_ios}"><img src="{url}img/download-ios.png" width="180"></a>
			</div>
		</div>	
		<div class="space">
		</div>									
	</div>
</div>  
{footer}		
</body>
</html>