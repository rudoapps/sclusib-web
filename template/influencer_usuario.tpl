{head}
<body>
<div class="header_static color_back">	
	<div class="container">
		{menu}
	</div>
</div>
<div class="main_section color_back">
	<div class="container">
		<div class="space"></div>		        
		<div class="row">		        
		    <div class="col-md-12 center">
		    	<img src="{url}img/influencer/usuario/GRACIAS!.png" class="influencer_responsive_small_img">
		    	<div class="space"></div>
		        <div id="influencer_usuario">		        	
			        			        		
			        <p>
			        ¡Nos encanta que quieras ser parte de nuestra familia en sclusib!
			        </p>
			        <p>
			        En esta etapa estamos buscando perfiles de más de 10.000 seguidores, pero al ritmo que llevas parece que lo conseguirás muy pronto.
			        </p>
			        <p>
			        ¡Déjanos tus datos y nos pondremos en contacto contigo, te seguiremos la pista!
			        </p>
			        <br/>
			        <form method="post" id="usuario" action="{url}unete/enviar">
			        <input type="text" class="telefono" name="phone"  placeholder="Escribe aquí tu número de teléfono..." required>
			        <input type="text" class="email" name="email" placeholder="Escribe aquí tu número de correo..." required>
			        </form>
			        
		        </div>
				<br/>
		        <button class="super button usuario">ENVIAR</button>
		        <div class="p40"></div>		        
		        <div class="p40"></div>
		    </div>
		</div>			
	</div>
</div>
{footer}
		
</body>
</html>