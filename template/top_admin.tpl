{head}
<body>
{menu_admin}
<div class="main_section color_front">
	<div class="container">	
		<div class="space"></div>
		<button type="button" class="btn btn-default  btn-sm fright add">
		 	<span class="glyphicon glyphicon-plus-sign" aria-hidden="true"></span> 
		</button>
		
		<div class="new-inf">
			<a href="#" class="add fright"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></a>
			<p class="size12 roman">Escribe el nombre del influencer: </p>
			<form method="post" action="{url}top-admin/newinfluencer" id="newInfluencer">
				<input type="text" name="newUsername">
				<button type="button" class="btn btn-success  btn-sm" id="accept">
					ACEPTAR    <span class="glyphicon glyphicon-send" aria-hidden="true"></span> 
				</button>	
			</form>
		</div>	

		<div class="new-desc">
			<a href="#" class="change fright"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></a>
			<div class="top-card-img img-desc">
        			<img src="" width="20">
        	</div>
        	<div class="usernametop">{username}</div>
			<p class="size12 roman">Escribe la descripción: </p>
			<form method="post" action="{url}top-admin/cambiar-descripcion" id="newDesc">
				<textarea name="newDesc" class="newDesc"></textarea>
				<input type="hidden" name="idDesc" class="itemID" value="">
				<input type="hidden" name="posDesc" class="itemPOS" value="">
				<button type="button" class="btn btn-success  btn-sm" id="accept-desc">
					ACEPTAR    <span class="glyphicon glyphicon-send" aria-hidden="true"></span> 
				</button>	
			</form>
		</div>	

		<button type="button" class="btn btn-default  btn-sm accept-list">
					REORGANIZAR    <span class="glyphicon glyphicon-sort" aria-hidden="true"></span> 
				</button>		
		
			<form method="post" action="{url}top-admin/newlist" id="newList">
			<!-- FORMULARIO DE NOMBRE LISTA-->
					<div class="new-list">					
						<a href="#" class="accept-list fright"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></a>
						<center>
							<button type="button" class="btn btn-default  btn-sm accept-newlist">
								NO QUIERO NOMBRE    <span class="glyphicon glyphicon-ok" aria-hidden="true"></span> 
							</button>	
						</center>
							<div class="hline"></div>
						<p class="size12 roman">Escribe el nombre de la lista: </p>						
							<input type="text" name="newListName">
							<button type="button" class="btn btn-success  btn-sm accept-newlist">
								ACEPTAR    <span class="glyphicon glyphicon-send" aria-hidden="true"></span> 
							</button>	
						
					</div>
			<div id="top">
				<ul id="sortable">
						{top}
				</ul>								
			</div>				
			</form>
	</div>
</div>  
{footer}		
</body>
</html>