{head}
<body>
<div class="header_static">	
	<div class="container">
		{menu}
	</div>
</div>
<div class="main_section color_front relative">
	<div class="container">															
			<div class="row">			
				<div class="col-md-12 center member team_space">
					<h1>El equipo</h1>
					<div class="team_line"></div>				
				</div>
			</div>
			<div class="row">			
				<div class="col-md-12 center member">
					<h2>NEGOCIO</h2>				
				</div>
			</div>
			<div class="row">
				<div class="col-xs-6 col-sm-4 center member">					
						<div class="round">
		        			<img src="{url}img/equipo/Richard.jpg" width="300px"></a>
		        		</div>

						<h3>RICHARD MORLA</h3>
						<div class="team_line"></div>
						<h3>CEO</h3>
						
						<p class="roman  left box">
							Le encanta crear negocios. MBA Internacional, 10 años de experiencia en marketing de gran consumo y tres empresas en su mochila.
						</p>
						<div class="box-social">
							<div class="row">
								<div class="col-md-12 center">
									<a href="https://es.linkedin.com/in/richardmorla/es" target="_blank"><img src="{url}img/social/linkedin.png">	</a>
								</div>						
							</div>
						</div>					
				</div>
				<div class="col-xs-6 col-sm-4 center member">
						<div class="round">
		        			<img src="{url}img/equipo/Marcos.jpg" width="300px"></a>
		        		</div>

						<h3>MARCOS PLAZAS</h3>
						<div class="team_line"></div>
						<h3>CDO</h3>

						<p class="roman  left box">
							El creativo. Convierte las ideas en UX. Diseñador gráfico formado en la Escuela de Arte Superior y Diseño de Valencia y en la universidad HAW Hamburg. 
						</p>
						<div class="box-social">
							<div class="row">
								<div class="col-md-12 center">
									<a href="https://es.linkedin.com/in/marcosplazas" target="_blank"><img src="{url}img/social/linkedin.png">	</a>
								</div>						
							</div>
						</div>
				</div>
				<div class="col-xs-6 col-sm-4 center member">
						<div class="round">
		        			<img src="{url}img/equipo/Pablo.jpg" width="300px"></a>
		        		</div>

						<h3>PABLO VILLAMAYOR</h3>
						<div class="team_line"></div>
						<h3>CONTENT</h3>

						<p class="roman  left box">
							Maneja Instagram, Twitter y Snapchat casi sin manos. Licenciado en Comunicación Audiovisual y encargado de todos los contenidos de sclusib. 
						</p>
						<div class="box-social">
							<div class="row">
								<div class="col-md-12 center">
									<a href="https://es.linkedin.com/in/pablo-villamayor-morales-372687104" target="_blank"><img src="{url}img/social/linkedin.png">	</a>
								</div>						
							</div>
						</div>
				</div>
			</div>
			<div class="row">			
					<div class="col-md-12 center member">
						<h2>DESARROLLO</h2>				
					</div>
			</div>
			<div class="row">
					<div class="col-xs-6 col-sm-4 center member">
							<div class="round">
			        			<img src="{url}img/equipo/Fernando.jpg" width="300px"></a>
			        		</div>

							<h3>FERNANDO SALOM</h3>
							<div class="team_line"></div>
							<h3>CTO</h3>

							<p class="roman size12 left box">
								Ingeniero superior. Maneja IOS, Android y back. Después de un MBA en USA y China trabajó en IBM pero lo dejó para montar la mejor app de la historia. 
							</p>
							<div class="box-social">
								<div class="row">
									<div class="col-md-12 center">
										<a href="https://es.linkedin.com/in/fsalom/es" target="_blank"><img src="{url}img/social/linkedin.png">	</a>
									</div>						
								</div>
							</div>
					</div>
					<div class="col-xs-6 col-sm-4 center member">
							<div class="round">
			        			<img src="{url}img/equipo/Oscar.jpg" width="300px"></a>
			        		</div>

							<h3>OSCAR VERA</h3>
							<div class="team_line"></div>
							<h3>ANDROID</h3>

							<p class="roman size12 left box">
								El único senior de Android con 20 años. Es el rey de los frameworks, las librerías y los fragments. Ama Google pero se acaba de pasar a Mac. 
							</p>

							<div class="box-social">
								<div class="row">
									<div class="col-md-12 center">
										<a href="https://es.linkedin.com/in/oscarveratomas" target="_blank"><img src="{url}img/social/linkedin.png">	</a>
									</div>						
								</div>
							</div>
					</div>
					<div class="col-xs-6 col-sm-4 center member">
							<div class="round">
			        			<img src="{url}img/equipo/Emilio.jpg" width="300px"></a>
			        		</div>

							<h3>EMILIO CARRIÓN</h3>
							<div class="team_line"></div>
							<h3>API & BACKEND</h3>

							<p class="roman size12 left box">
								 El genio de los datos. Empezó a programar a los 13 años y ahora le da a Django con un expediente académico excelente en Ingeniería Informática. 
							</p>
							<div class="box-social">
								<div class="row">
									<div class="col-md-12 center">
										<a href="https://es.linkedin.com/in/emilio-carrión-500850115/es" target="_blank"><img src="{url}img/social/linkedin.png">	</a>
									</div>						
								</div>
							</div>
					</div>
							
			</div>			
		</div>
    </div>   
</div>  
{footer}
		
</body>
</html>