			<li class="ui-state-default">
			<div class="top-card">			
				<div class="top-card-pos">
					{pos_number}
				</div>
				<div class="top-card-img">
        			<img src="{img_profile}" width="20">
        		</div>
        		<div class="roman size12">
        			
        			<span class="glyphicon {status} fright" aria-hidden="true"></span> 
					<b class="size15"><a href="https://www.instagram.com/{username}" target="_blank"><div class="username">{username}</div></a></b>{isSclusiber}
					Followers: {followers}<br/>
					<div class="desc">{desc}</div><br/>
					<div class="card-options">
						<a href="{url}top-admin/reload-{username}" >recargar datos</a> | 
						<a href="#" class="change">cambiar descripción</a> | 
						<a href="{url}top-admin/delete-{id_instagram}" class="delete">eliminar</a>					
					</div>
				</div>
				<div class="clear"></div>
				<input type="hidden" name="id[]" class="selectorID" value="{id_instagram}">
				<input type="hidden" name="pos[]" class="selectorPOS" value="{pos}">
			</div>
			</li>