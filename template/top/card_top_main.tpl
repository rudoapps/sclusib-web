		<div class="ranking-card">
			<div class="col1">			
				<div class="ranking-img">
        			<a href="https://www.instagram.com/{username}" target="_blank"><img src="{img_profile}"></a>
        		</div>
				<div class="ranking-pos">
					#{pos_number}
				</div>
			</div>
			<div class="col2 size12 roman">

				<div class="name"><a href="https://www.instagram.com/{username}" target="_blank"><div class="username">{username}</div></a></div>{isSclusiber}
				<div class="clear"></div>
				<div class="followers">{followers} seguidores en <b>instagram</b></div>
				<div class="desc">{desc}</div><br/>
			</div>
			
				<img src="{url}/img/ranking/{status}" align="right" class="ranking-position">

			<div class="clear"></div>
		</div>		