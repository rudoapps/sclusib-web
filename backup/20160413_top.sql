CREATE TABLE `sb_top` (
  `id` int(11) NOT NULL,
  `id_instagram` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `isOK` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `sb_top`
--

INSERT INTO `sb_top` (`id`, `id_instagram`, `username`, `isOK`, `created_at`) VALUES
(8, 12302845, 'fsalom', 1, '2016-04-12 18:30:45'),
(9, 2147483647, 'oscarveratomas', 1, '2016-04-12 21:32:30'),
(10, 375494734, 'richardmorla', 1, '2016-04-12 21:34:10'),
(12, 446099617, 'jonanwiergo', 1, '2016-04-12 23:29:10'),
(13, 1602461277, 'alonsoorenes', 1, '2016-04-12 23:36:23'),
(17, 623996105, 'oficial_jmartin', 1, '2016-04-13 12:05:03'),
(23, 231005142, 'personsmiling', 1, '2016-04-13 12:39:42'),
(25, 316199672, 'marcplazas', 1, '2016-04-13 12:45:58'),
(27, 1834084, 'anisal_', 1, '2016-04-13 16:56:47');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sb_top_position`
--

CREATE TABLE `sb_top_position` (
  `id` int(11) NOT NULL,
  `id_instagram` int(11) NOT NULL,
  `position` int(11) NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=69 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `sb_top_position`
--

INSERT INTO `sb_top_position` (`id`, `id_instagram`, `position`, `created_at`) VALUES
(11, 2147483647, 0, '2016-04-13 11:34:16'),
(12, 12302845, 1, '2016-04-13 11:34:16'),
(13, 375494734, 2, '2016-04-13 11:34:16'),
(14, 446099617, 3, '2016-04-13 11:34:16'),
(15, 1602461277, 4, '2016-04-13 11:34:16'),
(16, 2147483647, 0, '2016-04-13 11:39:21'),
(17, 12302845, 1, '2016-04-13 11:39:21'),
(18, 375494734, 2, '2016-04-13 11:39:21'),
(19, 446099617, 3, '2016-04-13 11:39:21'),
(20, 1602461277, 4, '2016-04-13 11:39:21'),
(21, 12302845, 0, '2016-04-13 11:44:14'),
(22, 2147483647, 1, '2016-04-13 11:44:14'),
(23, 375494734, 2, '2016-04-13 11:44:14'),
(24, 446099617, 3, '2016-04-13 11:44:14'),
(25, 1602461277, 4, '2016-04-13 11:44:14'),
(26, 375494734, 0, '2016-04-13 11:53:22'),
(27, 12302845, 1, '2016-04-13 11:53:22'),
(28, 2147483647, 2, '2016-04-13 11:53:22'),
(29, 446099617, 3, '2016-04-13 11:53:22'),
(30, 1602461277, 4, '2016-04-13 11:53:22'),
(31, 446099617, 0, '2016-04-13 11:56:11'),
(32, 1602461277, 1, '2016-04-13 11:56:11'),
(33, 375494734, 2, '2016-04-13 11:56:11'),
(34, 12302845, 3, '2016-04-13 11:56:11'),
(35, 2147483647, 4, '2016-04-13 11:56:11'),
(38, 623996105, 5, '2016-04-13 12:05:08'),
(39, 446099617, 0, '2016-04-13 12:05:15'),
(40, 1602461277, 1, '2016-04-13 12:05:15'),
(41, 623996105, 2, '2016-04-13 12:05:15'),
(42, 375494734, 3, '2016-04-13 12:05:15'),
(43, 12302845, 4, '2016-04-13 12:05:15'),
(44, 2147483647, 5, '2016-04-13 12:05:15'),
(50, 231005142, 6, '2016-04-13 12:39:47'),
(52, 446099617, 0, '2016-04-13 12:41:34'),
(53, 1602461277, 1, '2016-04-13 12:41:34'),
(54, 623996105, 2, '2016-04-13 12:41:34'),
(55, 231005142, 3, '2016-04-13 12:41:34'),
(56, 375494734, 4, '2016-04-13 12:41:34'),
(57, 12302845, 5, '2016-04-13 12:41:34'),
(58, 2147483647, 6, '2016-04-13 12:41:34'),
(59, 316199672, 7, '2016-04-13 12:46:02'),
(60, 446099617, 0, '2016-04-13 16:56:01'),
(61, 1602461277, 1, '2016-04-13 16:56:01'),
(62, 623996105, 2, '2016-04-13 16:56:01'),
(63, 231005142, 3, '2016-04-13 16:56:01'),
(64, 375494734, 4, '2016-04-13 16:56:01'),
(65, 316199672, 5, '2016-04-13 16:56:01'),
(66, 12302845, 6, '2016-04-13 16:56:01'),
(67, 2147483647, 7, '2016-04-13 16:56:01'),
(68, 1834084, 8, '2016-04-13 16:56:52');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sb_user_update`
--

CREATE TABLE `sb_user_update` (
  `id` int(11) NOT NULL,
  `id_instagram` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `biography` text NOT NULL,
  `followers` int(11) NOT NULL,
  `picture` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `sb_user_update`
--

INSERT INTO `sb_user_update` (`id`, `id_instagram`, `username`, `biography`, `followers`, `picture`, `created_at`) VALUES
(1, 12302845, 'fsalom', '', 139, 'https://scontent-mad1-1.cdninstagram.com/t51.2885-19/s150x150/12819041_1014887478559054_585514056_a.jpg', '2016-04-12 18:30:47'),
(2, 2147483647, 'oscarveratomas', '', 61, 'https://scontent-mad1-1.cdninstagram.com/t51.2885-19/s150x150/12224159_178227895861442_680440625_a.jpg', '2016-04-12 21:32:32'),
(3, 375494734, 'richardmorla', '', 292, 'https://scontent-mad1-1.cdninstagram.com/t51.2885-19/s150x150/11363971_1609161766017620_1124042269_a.jpg', '2016-04-12 21:34:13'),
(5, 446099617, 'jonanwiergo', '', 206963, 'https://scontent-lhr3-1.cdninstagram.com/t51.2885-19/s150x150/10369549_249882792023458_2122088551_a.jpg', '2016-04-12 23:29:13'),
(6, 1602461277, 'alonsoorenes', '', 71656, 'https://scontent-lhr3-1.cdninstagram.com/t51.2885-19/s150x150/12934850_1065563996840887_414431085_a.jpg', '2016-04-12 23:36:26'),
(10, 623996105, 'oficial_jmartin', '', 24953, 'https://scontent-lhr3-1.cdninstagram.com/t51.2885-19/s150x150/12224565_1504061216556484_1746943263_a.jpg', '2016-04-13 12:05:06'),
(16, 231005142, 'personsmiling', '', 9882, 'https://scontent-lhr3-1.cdninstagram.com/t51.2885-19/s150x150/11849189_1670631439818470_2088347175_a.jpg', '2016-04-13 12:39:45'),
(18, 316199672, 'marcplazas', '', 245, 'https://scontent-lhr3-1.cdninstagram.com/t51.2885-19/s150x150/11325457_1469034970083689_874768257_a.jpg', '2016-04-13 12:46:00'),
(19, 1834084, 'anisal_', '', 206, 'https://scontent-lhr3-1.cdninstagram.com/t51.2885-19/s150x150/12905043_824295074343187_520496222_a.jpg', '2016-04-13 16:56:49');


--
-- Indices de la tabla `sb_top`
--
ALTER TABLE `sb_top`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_instagram` (`id_instagram`);

--
-- Indices de la tabla `sb_top_position`
--
ALTER TABLE `sb_top_position`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `sb_user_update`
--
ALTER TABLE `sb_user_update`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

ALTER TABLE `sb_top`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT de la tabla `sb_top_position`
--
ALTER TABLE `sb_top_position`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=69;
--
-- AUTO_INCREMENT de la tabla `sb_user_update`
--
ALTER TABLE `sb_user_update`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=20;