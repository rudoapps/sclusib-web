<?php
	session_start();
	error_reporting(E_ALL);
	include_once("magic/class.SECURITY.php");
	include_once("magic/class.RENDER.php");
	include_once("magic/class.ADMIN.php");
	include_once("magic/class.TOP.php");
	include_once("magic/config.php");
	include_once("magic/define.php");
	$magic    = new render();
	$admin 	  = new admin();
	$top   	  = new top();
	$security = new security();

	$tags['url']='/sclusib/';	
	$tags['footer'] 	 = $magic->template($tags,"template/general/footer.tpl");		
	$tags['menu'] 		 = $magic->template($tags,"template/general/menu.tpl");

	if($admin->moreThan8())
		$tags['see_all'] = $magic->template($tags,"template/index/see_all.tpl");
	else
		$tags['see_all'] = "";

	if(!isset($_GET['go'])){				
		$tags['influencers'] = $admin->getInfluencers();		
		$tags['url'] 		 = '';
		$tags['url-website'] = _URL;
		$tags['footer'] 	 = $magic->template($tags,"template/general/footer.tpl");	
		$tags['menu'] 		 = $magic->template($tags,"template/general/menu.tpl");		
		$tags['head'] 		 = $magic->template($tags,"template/general/head.tpl");
		$template 	 		 = "template/index.tpl";
	}else{
		if(isset($_GET['name'])){ $influencer=$_GET['name'];}
		else{$influencer=null;}
		//echo $_GET['name'];
		$url		 		 ='../';		
		$tags['url-website'] = _URL;
		$tags['url']		 = $url;
		$tags['head'] 		 = $magic->template($tags,"template/general/head.tpl");	
		switch($_GET['go']){

			default:
				//404
				header('Location: '._URL."404/");	
			break;
			case 'equipo':
				$template = "template/team.tpl";							
			break;
			case 'login':
				$security->auth();
				header('Location: '._URL."admin/");					
			break;
			case 'logout':
				session_destroy();
				header('Location: '._URL."admin/");					
			break;
			case '404':															
				$template = "template/404.tpl";						
			break;
			case '401':															
				$template = "template/401.tpl";						
			break;
			case 'documents':
				$url		 		 ='';
				$tags['url']		 = $url;
				$template			 = "template/documents.tpl";
				$tags['footer'] 	 = $magic->template($tags,"template/general/footer.tpl");	
				$tags['menu'] 		 = $magic->template($tags,"template/general/menu.tpl");				
				$tags['head'] 		 = $magic->template($tags,"template/general/head.tpl");	
				if(isset($_GET['action'])){						
					switch ($_GET['action']){
						case 'terminos':					
							$tags['content'] = $magic->template($tags,"template/documents/terminos-y-condiciones.tpl");	
						break;	

						case 'privacy':
							$tags['content'] = $magic->template($tags,"template/documents/privacy.tpl");
						break;															
					}
				}
			break;
			case 'unete':				
				$tags['influencers'] = $admin->getInfluencers();	
				$template = "template/influencer_main.tpl";	
				if(isset($_GET['action'])){		
					$security->isInstagramAccess();				
					switch ($_GET['action']) {
						case 'influencer':		
							$template = "template/influencer_top.tpl";	
						break;						
						case 'user':	
							$template = "template/influencer_usuario.tpl";	
						break;
						case 'inscribirme':
							$admin->addRegister();
							header('Location: '._URL."unete/gracias");	
						break;
						case 'enviar':	
							$admin->addContact();
							header('Location: '._URL."unete/gracias");	
						break;
						case 'gracias':	
							$template = "template/gracias.tpl";
						break;
						case 'acuerdo':								
							$tags['content'] = $magic->template($tags,"template/documents/acuerdo-de-partner.tpl");
							$template			 = "template/documents.tpl";
						break;
						case 'preguntas':
							$tags['content'] = $magic->template($tags,"template/documents/preguntas-frecuentes.tpl");
							$template			 = "template/documents.tpl";
						break;
					}
				}
			break;
			case 'talent':		
				if($admin->isInfluencer($influencer)){
					$url		 		 ='';						
					$tags 				 = $admin->getData($influencer);				
					$tags['url']		 = $url;				
					$tags['text']		 = nl2br($tags['text']);				
					$tags['url-website'] = _URL;

					$template 			 = "template/influencer.tpl";
					$tags['footer'] 	 = $magic->template($tags,"template/general/footer.tpl");	
					$tags['menu'] 		 = $magic->template($tags,"template/general/menu.tpl");
					$tags['head'] 		 = $magic->template($tags,"template/general/head.tpl");
				}else{
					header('Location: '._URL."404/");	
				}
			break;
			case 'top100':			
				$tags['top']		 = $top->getTop(false);
				$tags['list']		 = $top->getList();			
				$tags['url-website'] = _URL;
				$tags['url']		 = $url;
				$tags['meta']		 = $magic->template($tags,"template/metas/top100.tpl");
				$tags['head'] 		 = $magic->template($tags,"template/general/head.tpl");
				$template = "template/top_main.tpl";						
			break;
			case 'register':			
				$tags['print']		 = $admin->printRegister();				
				$tags['url-website'] = _URL;
				$tags['url']		 = $url;				
				$tags['head'] 		 = $magic->template($tags,"template/general/head.tpl");
				$template = "template/register/index.tpl";						
			break;
			case 'admin':				
				if($security->isLogged()){
				//index.php?go=admin					
				if(isset($_GET['section'])){					
					$tags['head'] 		 = $magic->template($tags,"template/general/head.tpl");
					$tags['footer'] 	 = $magic->template($tags,"template/general/footer.tpl");	
					$tags['user_login']	 = $_SESSION['login_username'];					
					$tags['menu_admin']	 = $magic->template($tags,"template/admin/menu.tpl");						
					switch ($_GET['section']) {
						case 'top':
							//index.php?go=admin&section=top
							if(isset($_GET['action'])){
									
								switch ($_GET['action']) {
									case 'new':					
										//index.php?go=admin&section=top&action=new
										if($top->addTop($_POST['newUsername']))
											header('Location: '._URL."top-admin/");
										break;						
									case 'newlist':	
										//index.php?go=admin&section=top&action=newlist																				
										if($top->addList())
											header('Location: '._URL."top-admin/");
										break;
									case 'update':	
										//index.php?go=admin&section=top&action=update				
										if($top->addUserProfile($_GET['name']))
											header('Location: '._URL."top-admin/");
										break;
									case 'delete':	
										//index.php?go=admin&section=top&action=delete					
										if($top->deleteUser($_GET['name']))
											header('Location: '._URL."top-admin/");
										break;
									case 'desc':	
										//index.php?go=admin&section=top&action=desc					
										if($top->addDesc())
											header('Location: '._URL."top-admin/");
										break;
									case 'admin':	
										//index.php?go=admin&section=top&action=admin		
									break;
								}								
							}
							$tags['top']		 = $top->getTop(true);			
							$tags['url-website'] = _URL;
							$tags['url']		 = $url;								
							$template = "template/top_admin.tpl";
						break;
						
						case 'influencer':
							//index.php?go=admin&section=influencer							
							if(isset($_GET['action'])){	
								$tags = $admin->getData($influencer);											
								$tags['head'] 		 = $magic->template($tags,"template/general/head.tpl");
								$tags['footer'] 	 = $magic->template($tags,"template/general/footer.tpl");	
								$tags['user_login']	 = $_SESSION['login_username'];					
								$tags['menu_admin']	 = $magic->template($tags,"template/admin/menu.tpl");					
								
								$tags['select'] 	 = $admin->getSelectInfluencer($influencer);
								$tags['action'] 	 = $admin->getAction($_GET['action'],$influencer);				
								$tags['url-website'] = _URL;
								$tags['url']		 = $url;
								$template = "template/admin.tpl";
								switch ($_GET['action']) {																		
									case 'admin':	
										//index.php?go=admin&section=influencer&action=admin	
									break;
									case 'update':		
										//index.php?go=admin&section=influencer&action=update
										$admin->setData($influencer);	
										header('Location: '._URL."admin/".$_POST['username']);										
									break;
									case 'insert':					
										//index.php?go=admin&section=influencer&action=insert
										if($_POST['name']==null){
											header('Location: '._URL."magic/");	
										}				
										$admin->addData($_POST);
										header('Location: '._URL."admin/".$_POST['username']);
									break;
									case 'delete':	
										//index.php?go=admin&section=influencer&action=delete				
										$admin->deleteData($influencer);		
										header('Location: '._URL."admin/");										
									break;
								}
							}
						break;						
					}					
				}
				}else{
					$template 			 = "template/admin_main.tpl";
					$tags['menu'] 		 = "";					
					$tags['head'] 		 = $magic->template($tags,"template/general/head.tpl");
					$tags['login']		 = $magic->template($tags,"template/admin/login.tpl");
				}
			break; 			
			
		}
	}

	$code=$magic->template($tags,$template);
	echo $code;
?>