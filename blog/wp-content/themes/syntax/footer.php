<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content after
 *
 * @package Syntax
 */
?>



	</div><!-- #main -->
	<!--
	<footer id="colophon" class="site-footer" role="contentinfo">
		<?php if ( has_nav_menu( 'secondary' ) ) : ?>
			<div class="secondary-navigation">
				<?php wp_nav_menu( array( 'theme_location' => 'secondary', 'depth' => 1 ) ); ?>
			</div>
		<?php endif; ?>
		<div class="site-info">
			<?php do_action( 'syntax_credits' ); ?>
			<a href="http://wordpress.org/" title="<?php esc_attr_e( 'A Semantic Personal Publishing Platform', 'syntax' ); ?>" rel="generator"><?php printf( __( 'Proudly powered by %s', 'syntax' ), 'WordPress' ); ?></a>
			<span class="sep"> ~ </span>
			<?php printf( __( 'Theme: %1$s by %2$s.', 'syntax' ), 'Syntax', '<a href="https://wordpress.com/themes/" rel="designer">WordPress.com</a>' ); ?>
		</div>
	</footer>
	-->
	<div class="main_section color_footer relative">
	<div class="container">
		<div class="space p40"></div>
		<div class="row color_white roman size15">
			<div class="col-md-4">
			    <img src="<?php echo get_bloginfo('template_directory');?>/images/logo-sclusib-inverted.png">			   
			    <div class="space"></div>
			    <div class="line"></div>
			    	<div class="space"></div>
			    Consigue estar más cerca de tús ídolos.<br/>
			    ¡Ve contenido y gana premios!
			</div>
			<div class="col-md-8">
			    <div class="row">
			    	<div class="col-md-3 col-md-offset-1">		   	
			    		<ul class="nav-footer">
			    			<li><a href="#">Instagram</a></li>
			    			<li><a href="#">Twitter</a></li>
			    		</ul>
			    	</div>
			    	<div class="col-md-4">
			    		<ul class="nav-footer">
			    			<li><a href="#">Preguntas frecuentes</a></li>
			    			<li><a href="#">Nuestros talentos</a></li>
			    			<li><a href="#">Ayuda</a></li>
			    			<li><a href="#">Acerca de sclusib</a></li>
			    		</ul>
			    	</div>
			    	<div class="col-md-4">
			    		<ul class="nav-footer">
			    			<li><a href="#">Contacto</a></li>
			    			<li><a href="#">Cookies</a></li>
			    			<li><a href="#">Política de Privacidad</a></li>
			    			<li><a href="#">Términos de uso</a></li>
			    		</ul>
			    	</div>
			    </div>
			</div>

		</div>
		<div class="space p40"></div>			
	</div>
	<div class="box-send size14 roman">
		<img src="<?php echo get_bloginfo('template_directory');?>/images/ic_email.png" class="s-right"> <a href="#">¿Tienes alguna pregunta?</a>
	</div>
</div>
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>