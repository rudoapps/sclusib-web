<?php
/** 
 * Configuración básica de WordPress.
 *
 * Este archivo contiene las siguientes configuraciones: ajustes de MySQL, prefijo de tablas,
 * claves secretas, idioma de WordPress y ABSPATH. Para obtener más información,
 * visita la página del Codex{@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} . Los ajustes de MySQL te los proporcionará tu proveedor de alojamiento web.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** Ajustes de MySQL. Solicita estos datos a tu proveedor de alojamiento web. ** //
/** El nombre de tu base de datos de WordPress */
define('DB_NAME', 'sclusib');

/** Tu nombre de usuario de MySQL */
define('DB_USER', 'root');

/** Tu contraseña de MySQL */
define('DB_PASSWORD', '');

/** Host de MySQL (es muy probable que no necesites cambiarlo) */
define('DB_HOST', 'localhost');

/** Codificación de caracteres para la base de datos. */
define('DB_CHARSET', 'utf8mb4');

/** Cotejamiento de la base de datos. No lo modifiques si tienes dudas. */
define('DB_COLLATE', '');

/**#@+
 * Claves únicas de autentificación.
 *
 * Define cada clave secreta con una frase aleatoria distinta.
 * Puedes generarlas usando el {@link https://api.wordpress.org/secret-key/1.1/salt/ servicio de claves secretas de WordPress}
 * Puedes cambiar las claves en cualquier momento para invalidar todas las cookies existentes. Esto forzará a todos los usuarios a volver a hacer login.
 *
 * @since 2.6.0
 */
define('AUTH_KEY', 'XaGp-KE} ao$1@U&ARfkp5=1pRG`(VGOJ# &w<xi,Qnq<>M;PUo7IkSRy^F=,N -');
define('SECURE_AUTH_KEY', 'xC*~Q@Krg]+V(T1?#xr<H#Bi]z]9rsQsp`p92RsPOS_As1x) 7.UZ8?P]Z14d-z<');
define('LOGGED_IN_KEY', '$+] @ZO9<MSrX&MJqPuF(vmqe ae}J,=]6SCg8q%u46$gEhgivOS-Y%?X>EmHd6O');
define('NONCE_KEY', '~w_rAI=r.1n:3O58}_TM:]R  I*0Ln$</2#C+Qyaj`H4}BLB}+M[5S>@=^sA0gFt');
define('AUTH_SALT', 'EDr8OTq?VSo)_C)$B9EIsC)z4g[rK2iHd`V{4/Mud0_dw p~xstp0*=0,E(zvb/w');
define('SECURE_AUTH_SALT', 'Z*mxlODD6yTbmxQj(I(h=j8A6PFF=y[%H4F+D@F5h0_XNwe7&Z;kAi8(mknd02o%');
define('LOGGED_IN_SALT', '@WHH$^Nb&A0)!_9cx6M:c%=B2#HGrMpv<_;5:^7(c,D9I]MXO1,vb6m=;+y.SUHl');
define('NONCE_SALT', '+8j[R9*MGo9P(h?3?kQcMw9TD3`]<aIR|s@D{cK:Xk:@oVX!(v%1[X=QpyR@I$f-');

/**#@-*/

/**
 * Prefijo de la base de datos de WordPress.
 *
 * Cambia el prefijo si deseas instalar multiples blogs en una sola base de datos.
 * Emplea solo números, letras y guión bajo.
 */
$table_prefix  = 'wp_';


/**
 * Para desarrolladores: modo debug de WordPress.
 *
 * Cambia esto a true para activar la muestra de avisos durante el desarrollo.
 * Se recomienda encarecidamente a los desarrolladores de temas y plugins que usen WP_DEBUG
 * en sus entornos de desarrollo.
 */
define('WP_DEBUG', false);

/* ¡Eso es todo, deja de editar! Feliz blogging */

/** WordPress absolute path to the Wordpress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

